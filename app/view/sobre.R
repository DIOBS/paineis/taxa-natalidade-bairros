box::use(
  shiny[...],
  bs4Dash[...]
)

#' @export
ui <- function(id) {
  ns <- NS(id)

  fluidPage(
    h1("Sobre o painel"),
    p("Estimativa para o cálculo da população nos bairros de Fortaleza e taxas."),
    h1("Autor"),
    p(
      "Rômulo Andrade da Silva",
      a(
        href = "mailto:romulo.andrade@iplanfor.fortaleza.ce.gov.br",
        "<romulo.andrade@iplanfor.fortaleza.ce.gov.br>"
      ),
      br(),
      tags$i("Núcleo de Sala Situacional - IPLANFOR")
    ),
    h1("Contribuições"),
    p(
      "João Cláudio Nunes Carvalho",
      a(
        href = "mailto:joaoclaudio82@gmail.com",
        "<joaoclaudio82@gmail.com>"
      ),
      br(),
      tags$i("Programa Cientista Chefe - FUNCAP")
    ),    
    p(
      "Victor Santos",
      a(
        href = "mailto:victor.santos@iplanfor.fortaleza.ce.gov.br",
        "victor.santos@iplanfor.fortaleza.ce.gov.br"
      ),
      br(),
      tags$i("Observatório de Fortaleza - IPLANFOR")
    ),    
    h1("Código-Fonte"),
    p(
      a(
        href="https://gitlab.com/DIOBS/paineis/taxa-natalidade-bairros",
        "https://gitlab.com/DIOBS/paineis/taxa-natalidade-bairros"
      )
    )
  )
}

#' @export
server <- function(id) {
  moduleServer(id, function(input, output, session) {

  })
}
